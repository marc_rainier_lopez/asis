@ECHO OFF
SET BINDIR=%~dp0
CD /D "%BINDIR%
REM If this line below fails, replace it with the location of your java 7 install.
"C:\Program Files (x86)\Java\jre7\bin\javaw.exe" -Xmx1024M -Xms1024M -jar ASIS.jar
PAUSE